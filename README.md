#Deployment of OpenShift based infrastructure in different environments

1. ##Local development environment
    - OpenShift Origin all-in-one VM

        Read the instructions at [https://www.openshift.org/vm/](Link URL) and install the dependencies (VirtualBox and Vagrant), paying attention to the recommended/supported versions at the bottom of page.

        Make sure you have the Openshift CLI installed [https://docs.openshift.org/latest/cli_reference/get_started_cli.html#installing-the-cli](Link URL)


    - NSF shared folders.

        To keep state outside of the VM it's recommended to setup shared folder(s) between the host and the VirtualBox machine.

        The included Vagrantfile has been modified to setup an NFS shared folder, which can be used to store persistent volumes created in the OpenShift deployment.

        The "local" versions of the included persistent volume templates are already configured to use this folder as their root.

        NFS is strongly recommended as using normal VirtualBox shared folders can cause weird permission issues when Docker containers try to access them, especially when not running as the "root" user.


    - Start VM

        You will be asked to enter your admin password to be able to create the NFS shared folder!

            cd envs/local
            vagrant box add openshift/origin-all-in-one
            vagrant up --provider=virtualbox
            cd ../..

2. ##Git Repository

    - Using local Gogs for development (https://gogs.io/)

            oc login 10.2.2.2:8443

          **username**: admin

          **password**: admin

            oc new-project gogs --display-name="Gogs"
            oc create -f templates/gogs/gogs-sa.yml
            oc adm policy add-scc-to-user privileged system:serviceaccount:gogs:gogs
            oc create -f envs/local/volumes/gogs-pv.yml
            oc create -f templates/gogs/gogs-standalone-template.yml

          Open a browser and visit [https://10.2.2.2:8443/console/](Link URL) and login with admin/admin

          Open the Gogs project and click "Add to project".

          Select the "gogs-standalone-template" and change the "Application Hostname" parameter to "gogs.10.2.2.2.xip.io".

          Click "Create" and wait for the deployment to be completed.

          Browse to [http://gogs.10.2.2.2.xip.io](Link URL) to complete installation.

          Select SQLite as db, domain=gogs.10.2.2.2.xip.io and "Application URL"=http://gogs.10.2.2.2.xip.io:3000/

          Register a Gogs user.

          The first registered user will have administrative privileges.

          Create the "RandomIdea" organisation from the Gogs web interface.

          To make Gogs ignore untrusted SSL certs for webhooks edit its config file inside the shared folder "persisted/pv-gogs/gogs/conf/app.ini", set SKIP_TLS_VERIFY to true and trigger a new deployment in the Gogs project:

            [webhooks]
            SKIP_TLS_VERIFY=true

    - Using deploy keys for Gogs repos

          Follow the instructions for generating a new SSH key: [https://help.github.com/articles/generating-an-ssh-key/](Link URL)
          Add the private part of the generated key for **EVERY** OpenShift project where the repo will be accessed:

            oc secrets new-sshauth sshsecret --ssh-privatekey=<path to private key>
            oc secrets add serviceaccount/builder secrets/sshsecret

          In the repository settings in Gogs UI add a "Deploy Key", which is the public part of the SSH key generated from previous step.


    - Using a private repo at GitHub or Bitbucket

        https://blog.openshift.com/deploying-from-private-git-repositories/

        **TODO** Need more details and link to the private key we use.


3. ##Deploying the games backend

    The backend consists of multiple services running inside separate Docker containers.

    - Using Gogs repositories for local development

        The code for the backend services is spread across multiple Git repositories, which can be linked to the local Gogs installation  for triggering builds through webhooks in the local cluster.

       * Add a repo owned by the RandomIdea organisation through the web UI.
       * Add the new repo as a "remote" (for example called "local") to the local repository where you cloned the code.
       * Push the local repo to the Gogs remote


    - Configuring build webhooks

        OpenShift supports triggering builds when new code is pushed to the repos by using Web hooks. GitHub, Bitbucket and Gogs all support setting the webhooks for a repo from its settings page.
        To find the Webhook URL of a specific build:

            * Select Builds->Builds from the project's side menu
            * Click on the link for the build in the builds list
            * Open the Configuratio tab and copy "Generic webhook URL"

        Alternatively look for "Webhook Generic" in the output of the command:

                oc describe bc <build name>


    - Create "backend" project

            oc new-project backend --display-name="Backend"


    - Load configs
        
            oc create -f envs/<environment>/configs/backend-configs.yml


    - Load secrets 

            oc create -f envs/<environment>/secrets/mysql-secrets.yml
            oc create -f envs/<environment>/secrets/aws-secrets.yml

        Secret values must be base64 encoded.
        On OSX this will output the correct string:

            echo -n "mysql" | openssl base64


    - Setup persistent volumes (example bellow is for local, configure corresponding volumes for other environments as per the platform, i.e. using EBS on Amazon etc.) if deploying a custom MySQL or Redis server. Skip this step if using managed services like RDS and ElastiCache.

            oc create -f envs/<environment>/volumes/mysql-pv.yml
            oc create -f envs/<environment>/volumes/redis-pv.yml


    - Setup local MySQL DB (if running locally)

            oc create -f templates/mysql/persistent-mysql.json

           Open the OpenShift console, visit the Backend project and click "Add to project".
           Select the "backend-mysql" template and follow the wizard.


    - Setup local Redis (if running locally)

            oc create -f templates/redis/persistent-redis.json

          Open the OpenShift console, visit the Backend project and click "Add to project".
          Select the "backend-redis" template and follow the wizard.


    - Setup database migrations

            oc create -f templates/backend/db-migrations/template.yml

          Open the OpenShift console, visit the Backend project and click "Add to project".
          Select the "db-migrations" template and follow the wizard.

          **Git Repo URL**: https://bitbucket.org/randomidea/backenddb (or use a corresponding Gogs URL for local development)
          
          
    - Setup NATS cluster

            oc create -f templates/nats/nats.yml
            
          Open the OpenShift console, visit the Backend project and click "Add to project".
          Select the "nats-service" template and follow the wizard.
          
          **Git Repo URL**: https://bitbucket.org/randomidea/nats (or use a corresponding Gogs URL for local development)
          
    - Setup the Whip service
    
            oc create -f envs/<environment>/configs/whip-configs.yml
            oc create -f templates/backend/whip/template.yml

          Open the OpenShift console, visit the Backend project and click "Add to project".
          Select the "whip" template and follow the wizard.

          **Git Repo URL**: https://bitbucket.org/randomidea/whip (or use a corresponding Gogs URL for local development)
          
    - Setup the push server
    
            oc create -f envs/<environment>/configs/push-configs.yml
            oc create -f envs/<environment>/secrets/push-secrets.yml
            oc create -f templates/backend/push-server/template.yml
    
          Open the OpenShift console, visit the Backend project and click "Add to project".
          Select the "push-server" template and follow the wizard.
    
          **Git Repo URL**: https://bitbucket.org/randomidea/push_server (or use a corresponding Gogs URL for local development)
        
    - Setup API Gateway

            oc create -f templates/backend/api-gateway/template.yml

          Open the OpenShift console, visit the Backend project and click "Add to project".
          Select the "api-gateway" template and follow the wizard.
          Make sure you edit the domain name to something appropriate (like "api.10.2.2.2.xip.io" for local dev).

          **Git Repo URL**: https://bitbucket.org/randomidea/ri_api (or use a corresponding Gogs URL for local development)


    - Setup a GRPC service

            oc create -f templates/backend/grpc/service-template.yml

           Most GRPC services will follow a similar pattern and can be deployed using the above template.
           Open the OpenShift console, visit the Backend project and click "Add to project".
           Select the "grpc-service" template and follow the wizard.

4. ##Service-specific notes

    - Auth service

           **Git Repo URL**: https://bitbucket.org/randomidea/user-services (or use a corresponding Gogs URL for local development)

           **Application Name**: "auth-grpc"

           **Service Settings**: {"mode": "auth"}

           Add label in **Labels** section:

           **app**: auth-grpc


    - Accounts service

           **Git Repo URL**: https://bitbucket.org/randomidea/user-services (or use a corresponding Gogs URL for local development)

           **Application Name**: "accounts-grpc"

           **Service Settings**: {"mode": "accounts"}

           Add label in **Labels** section:

           **app**: accounts-grpc

   - Admin service

          **Git Repo URL**: https://bitbucket.org/randomidea/admin-service (or use a corresponding Gogs URL for local development)

          **Application Name**: "admin-grpc"

          Add label in **Labels** section:

          **app**: admin-grpc


    - Appdata service

           **Git Repo URL**: https://bitbucket.org/randomidea/appdata-service (or use a corresponding Gogs URL for local development)

           **Application Name**: "appdata-grpc"

           Add label in **Labels** section:

           **app**: appdata-grpc

    - Inventoy service

           **Git Repo URL**: https://bitbucket.org/randomidea/inventory-service (or use a corresponding Gogs URL for local development)

           **Application Name**: "inventory-grpc"

           Add label in **Labels** section:

           **app**: inventory-grpc

     - Leaderboards service

            **Git Repo URL**: https://bitbucket.org/randomidea/leaderboards-service (or use a corresponding Gogs URL for local development)

            **Application Name**: "leaderboards-grpc"

            Add label in **Labels** section:

            **app**: leaderboards-grpc

      - Matches service

             **Git Repo URL**: https://bitbucket.org/randomidea/match-service (or use a corresponding Gogs URL for local development)

             **Application Name**: "matches-grpc"

             Add label in **Labels** section:

             **app**: matches-grpc
             
      - Location service

             **Git Repo URL**: https://bitbucket.org/randomidea/location-service (or use a corresponding Gogs URL for local development)

             **Application Name**: "location-grpc"

             Add label in **Labels** section:

             **app**: location-grpc

5. ##Deploying the Admin UI

    The admin UI is a Django(i.e. python) app, which provides high level, web UI for managing the backend DB.
    It needs a separate database for storing its internal data, like users, permissions etc.
    There are also several configurations stored as ConfigsMap and Secret objects specific to the Admin, which need to be setup before deployment.

    To create configs and secrets for local development:

        oc create -f envs/<environment>/configs/adminui-configs.yml
        oc create -f envs/<environment>/secrets/mysql-adminui-secrets.yml

    The Admin UI will also need a database with the configured name to be created at the configured host and the configured user having permissions to access it.

    For local development:

    - SSH to mysql pod (you need to replace the actual pod name in the rsh command!)

            oc get pods -l name=mysql
            oc rsh <mysql-pod-name>

           Login to mysql on the pod as root, create the DB and grant privileges to the user configured for the Admin UI:

            mysql -u root -h 127.0.0.1
            CREATE DATABASE adminui;
            GRANT ALL PRIVILEGES ON adminui.* TO 'mysql'@'%' WITH GRANT OPTION;
            exit
            exit

    - Start admin app from template:

            oc create -f templates/backend/admin-ui/template.yml

           Open the OpenShift console, visit the Backend project and click "Add to project".
           Select the "admin-ui" template and follow the wizard.

           Parameter values for the template:

           **Git Repo URL**: https://bitbucket.org/randomidea/ri_admin_ui (or use a corresponding Gogs URL for local development)

           **Application Name**: "admin-ui"

           **Application Hostname**: adminui.10.2.2.2.xip.io

           Add label in **Labels** section:

           **app**: admin-ui

    - Create Django admin user

           SSH to the pod running the Admin (you need to replace the actual pod name in the rsh command!)

            oc get pods -l app=admin-ui
            oc rsh <admin-ui-pod-name>

           Change the username/email/password in the command bellow if necessary and run:

            echo "from django.contrib.auth.models import User; User.objects.create_superuser('admin', 'admin@rndmi.com', 'admin')" | python manage.py shell

           Browse to the URL configured for the app's router and login with the username/password for the Django user created above.