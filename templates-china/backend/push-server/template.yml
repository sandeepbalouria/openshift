apiVersion: v1
kind: Template
metadata:
  name: push-server
  creationTimestamp: null
  annotations:
    description: "Deploy the Push server service"
    tags: "backend,${APPLICATION_NAME},push-server"
objects:
- apiVersion: v1
  kind: Service
  metadata:
    labels:
      app: "${APPLICATION_NAME}"
    name: "${APPLICATION_NAME}"
    creationTimestamp: null
  spec:
    ports:
    - name: "80-tcp"
      port: 80
      protocol: TCP
      targetPort: 8080
      nodePort: 0
    selector:
      name: "${APPLICATION_NAME}"
      app: "${APPLICATION_NAME}"
      deploymentconfig: "${APPLICATION_NAME}"
    sessionAffinity: None
    type: ClusterIP
- apiVersion: v1
  kind: ImageStream
  metadata:
    name: ${APPLICATION_NAME}
    creationTimestamp: null
  status: 
    dockerImageRepository: ""
- apiVersion: v1
  kind: BuildConfig
  metadata:
    name: "${APPLICATION_NAME}"
    creationTimestamp: null
    labels:
      name: "${APPLICATION_NAME}"
  spec:
    triggers:
      - type: "Generic"
        generic:
          secret: "7u$#_$4cr47"
      - type: "ImageChange"
      - type: "ConfigChange"
    source:
      type: "Git"
      git:
        uri: "${GIT_REPO}"
        ref: "${GIT_REPO_REF}"
      sourceSecret:
        name: "sshsecret"
    strategy:
      type: "Docker"
    output:
      to:
        kind: "ImageStreamTag"
        name: "${APPLICATION_NAME}:latest"
    pushSecret:
      name: "randompush"
- apiVersion: v1
  kind: DeploymentConfig
  metadata:
    labels:
      app: "${APPLICATION_NAME}"
    name: "${APPLICATION_NAME}"
  spec:
    replicas: 2
    selector:
      name: "${APPLICATION_NAME}"
      app: "${APPLICATION_NAME}"
      deploymentconfig: "${APPLICATION_NAME}"
    strategy:
      type: Rolling
      rollingParams:
        intervalSeconds: 1
        updatePeriodSeconds: 1
        timeoutSeconds: 120
    triggers:
    - type: ConfigChange
    - type: "ImageChange"
      imageChangeParams: 
        automatic: true
        containerNames:
        - "${APPLICATION_NAME}"
        from:
          kind: "ImageStreamTag"
          name: "${APPLICATION_NAME}:latest"
        lastTriggeredImage: ""
    template:
      metadata:
        creationTimestamp: null
        labels:
          name: "${APPLICATION_NAME}"
          app: "${APPLICATION_NAME}"
          deploymentconfig: "${APPLICATION_NAME}"
      spec:
        containers:
        - image: "${APPLICATION_NAME}"
          name: "${APPLICATION_NAME}"
          ports:
          - containerPort: 8080
            protocol: TCP
          volumeMounts:
          - name: push-secrets
            mountPath: /etc/push/secrets
            readOnly: true
          env:
          - name: "CONF_GCM_API_KEY"
            valueFrom:
              secretKeyRef:
                name: "push-secrets"
                key: "gcm.api.key"
          - name: "CONF_APPLE_PUSH_SANDBOX"
            valueFrom:
              configMapKeyRef:
                name: "push-configs"
                key: "apple.sandbox"
          terminationMessagePath: "/dev/termination-log"
          securityContext:
            privileged: false
        volumes:
          - name: push-secrets
            secret: 
              secretName: push-secrets
        restartPolicy: "Always"
        dnsPolicy: "ClusterFirst"
        terminationGracePeriodSeconds: 30
parameters: 
  - name: "GIT_REPO"
    displayName: "Git Repo URL"
    description: "The Git repo of the app"
    required: true
  - name: "GIT_REPO_REF"
    displayName: "Git Repo Reference"
    description: "The Git repo reference (i.e. branch, tag etc) to deploy"
    value: "master"
    required: true
  - name: "APPLICATION_NAME"
    displayName: "Application Name"
    description: "The name of the app"
    value: "push-server"
    required: true
