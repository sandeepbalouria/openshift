apiVersion: v1
kind: Template
metadata:
  name: whip
  creationTimestamp: null
  annotations:
    description: "Deploy the whip service"
    tags: "products,${APPLICATION_NAME},rpc"
objects:
- apiVersion: v1
  kind: Service
  metadata:
    labels:
      app: "${APPLICATION_NAME}"
    name: "${APPLICATION_NAME}"
    creationTimestamp: null
  spec:
    ports:
    - name: "8421-tcp"
      port: 8421
      protocol: TCP
      targetPort: 8421
      nodePort: ${NODE_PORT}
    selector:
      name: "${APPLICATION_NAME}"
      app: "${APPLICATION_NAME}"
      deploymentconfig: "${APPLICATION_NAME}"
    sessionAffinity: None
    type: NodePort
- apiVersion: v1
  kind: ImageStream
  metadata:
    name: ${APPLICATION_NAME}
    creationTimestamp: null
  status: 
    dockerImageRepository: ""
- apiVersion: v1
  kind: BuildConfig
  metadata:
    name: "${APPLICATION_NAME}"
    creationTimestamp: null
    labels:
      name: "${APPLICATION_NAME}"
  spec:
    triggers:
      - type: "Generic"
        generic:
          secret: "wh1p_$4cr47"
      - type: "ImageChange"
      - type: "ConfigChange"
    source:
      type: "Git"
      git:
        uri: "${GIT_REPO}"
        ref: "${GIT_REPO_REF}"
      sourceSecret:
        name: "sshsecret"
    strategy:
      type: "Docker"
    output:
      to:
        kind: "ImageStreamTag"
        name: "${APPLICATION_NAME}:latest"
    pushSecret:
      name: "randompush"
    
- apiVersion: v1
  kind: DeploymentConfig
  metadata:
    labels:
      app: "${APPLICATION_NAME}"
    name: "${APPLICATION_NAME}"
  spec:
    replicas: 2
    selector:
      name: "${APPLICATION_NAME}"
      app: "${APPLICATION_NAME}"
      deploymentconfig: "${APPLICATION_NAME}"
    strategy:
      type: Rolling
      rollingParams:
        intervalSeconds: 1
        updatePeriodSeconds: 1
        timeoutSeconds: 120
    triggers:
    - type: ConfigChange
    - type: "ImageChange"
      imageChangeParams: 
        automatic: true
        containerNames:
        - "${APPLICATION_NAME}"
        from:
          kind: "ImageStreamTag"
          name: "${APPLICATION_NAME}:latest"
        lastTriggeredImage: ""
    template:
      metadata:
        creationTimestamp: null
        labels:
          name: "${APPLICATION_NAME}"
          app: "${APPLICATION_NAME}"
          deploymentconfig: "${APPLICATION_NAME}"
      spec:
        containers:
        - image: "${APPLICATION_NAME}"
          name: "${APPLICATION_NAME}"
          ports:
          - containerPort: 8421
            protocol: TCP
          volumeMounts:
          - name: whip-configs
            mountPath: /etc/whip
          env:
          - name: "CONF_ORM_DEBUG"
            value: "${CONF_ORM_DEBUG}"
          - name: "CONF_DEFAULT_ORM_TYPE"
            value: "mysql"
          - name: "CONF_DEFAULT_ORM_HOST"
            valueFrom:
              configMapKeyRef:
                name: "backend-configs"
                key: "mysql-host"
          - name: "CONF_DEFAULT_ORM_PORT"
            valueFrom:
              configMapKeyRef:
                name: "backend-configs"
                key: "mysql-port"
          - name: "CONF_DEFAULT_ORM_NAME"
            valueFrom:
              configMapKeyRef:
                name: "backend-configs"
                key: "mysql-dbname"
          - name: "CONF_DEFAULT_ORM_USER"
            valueFrom:
              secretKeyRef:
                name: "mysql-secrets"
                key: "username"
          - name: "CONF_DEFAULT_ORM_PASSWORD"
            valueFrom:
              secretKeyRef:
                name: "mysql-secrets"
                key: "password"
          - name: "CONF_DEFAULT_ORM_MAX_IDLE"
            value: ${CONF_DEFAULT_ORM_MAX_IDLE}
          - name: "CONF_DEFAULT_ORM_MAX_CONN"
            value: ${CONF_DEFAULT_ORM_MAX_CONN}
          - name: "CONF_SESSION_PROVIDER"
            value: "ri_redis"
          - name: "CONF_CACHE_PROVIDER"
            value: "ri_redis"
          - name: "CONF_SESSION_SAVE_PATH"
            valueFrom:
              configMapKeyRef:
                name: "backend-configs"
                key: "redis-sessions-savepath"
          - name: "CONF_CACHE_ADDRESS"
            valueFrom:
              configMapKeyRef:
                name: "backend-configs"
                key: "redis-cache-address"
          - name: "CONF_CACHE_PASSWORD"
            valueFrom:
              configMapKeyRef:
                name: "backend-configs"
                key: "redis-cache-password"
          - name: "CONF_WHIP_MESSAGING_TRANSPORT"
            valueFrom:
              configMapKeyRef:
                name: "whip-configs"
                key: "messaging-transport"
          - name: "CONF_WHIP_PING_TIMEOUT"
            valueFrom:
              configMapKeyRef:
                name: "whip-configs"
                key: "ping-timeout"
          - name: "LOGS_LEVEL"
            value: ${CONF_LOGS_LEVEL}
          - name: "GO_RUN_MODE"
            value: "${CONF_RUN_MODE}"  
          terminationMessagePath: "/dev/termination-log"
          securityContext:
            privileged: false
        volumes:
        - name: whip-configs
          configMap:
            name: whip-configs
            items:
            - key: configs.json
              path: configs.json
        restartPolicy: "Always"
        dnsPolicy: "ClusterFirst"
        terminationGracePeriodSeconds: 30
        
parameters: 
  - name: "GIT_REPO"
    displayName: "Git Repo URL"
    description: "The Git repo of the app"
    required: true
  - name: "GIT_REPO_REF"
    displayName: "Git Repo Reference"
    description: "The Git repo reference (i.e. branch, tag etc) to deploy"
    value: "master"
    required: true
  - name: "APPLICATION_NAME"
    displayName: "Application Name"
    description: "The name of the app"
    value: "whip"
    required: true
  - name: "NODE_PORT"
    displayName: "Node Port"
    description: "The public port at which to expose the service across the cluster"
    value: "30201"
    required: true
  - name: "CONF_ORM_DEBUG"
    displayName: "Enable ORM Debuging"
    description: "Whether to enable orm debug logging."
    value: "false"
    required: true
  - name: "CONF_DEFAULT_ORM_MAX_IDLE"
    displayName: "Default DB max idle connections"
    description: "Maximum idle connections to the 'default' database."
    value: "3"
    required: true
  - name: "CONF_DEFAULT_ORM_MAX_CONN"
    displayName: "Default DB max connections"
    description: "Maximum connections to the 'default' database."
    value: "0"
    required: true
  - name: "CONF_RUN_MODE"
    displayName: "Run Mode"
    description: "One of 'dev' or 'prod'"
    value: "dev"
    required: true
  - name: "CONF_LOGS_LEVEL"
    displayName: "Logging Level"
    description: "The BeeGo logging level setting."
    value: "7"
    required: true
