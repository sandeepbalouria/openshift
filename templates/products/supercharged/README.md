#Deployment of Supercharged APIs at OpenShift based infrastructure

1. ##Local development environment
    
    - Create project
    
            oc new-project supercharged --display-name="SuperCharged"
    
    - Set ssh key for git 
    
            oc secrets new-sshauth sshsecret --ssh-privatekey=<path to private key>
            oc secrets add serviceaccount/builder secrets/sshsecret
            
   - Set configs and secrets
    
            oc create -f envs/<environment>/configs/products/supercharged/configs.yml
            oc create -f envs/<environment>/configs/products/supercharged/pvp-configs.yml
            oc create -f envs/<environment>/configs/products/supercharged/whip-configs.yml
            oc create -f envs/<environment>/secrets/products/supercharged/mysql-secrets.yml
            
    - Setup a SuperCharged service

            oc create -f templates/products/supercharged/template-api.yml
