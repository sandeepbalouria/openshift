#Deployment of Erating server at OpenShift based infrastructure

1. ##Local development environment
    
    - Create project
    
            oc new-project erating-forward --display-name="EratingForward"
    
    - Set ssh key for git 
    
            oc secrets new-sshauth sshsecret --ssh-privatekey=<path to github private key for RNDMI user>
            oc secrets add serviceaccount/builder secrets/sshsecret
                        
   - Set configs and secrets
    
            oc create -f envs/<envname>/configs/products/erating/configs.yml
            oc create -f envs/<envname>/secrets/products/erating/mongodb-secrets.yml
            oc create -f envs/<envname>/secrets/products/erating/erating-secrets.yml
            
    - Create volumes (not necessary for local, just use internal Openshift volumes)
    
            oc create -f envs/<envname>/volumes/products/erating/mongodb-pv.yml
            oc create -f envs/<envname>/volumes/products/erating/redis-pv.yml
            
    - Setup MongoDB for Erating project (for local/vagrant use OS's internal persistent volumes, mongo doesn't work with virtual box shared folders, i.e. pv01):
    
            oc create -f templates/mongodb/persistent-mongo.json
            
    - Setup Redis for Erating project (for local use the internal persistent volumes, i.e. pv02):
    
            oc create -f templates/redis/persistent-redis.json
            
    - Setup the erating service

            oc create -f templates/products/erating/template.yml
