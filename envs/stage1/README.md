To setup ssh secret for Github, Bitbucket or Gogs:

        oc secrets new-sshauth sshsecret --ssh-privatekey=$HOME/.ssh/<private_key>
        oc secrets add serviceaccount/builder secrets/sshsecret
        git:
             uri: "git@repository.com:user/app.git" 
           sourceSecret:
             name: "sshsecret"
           type: "Git" (edited)